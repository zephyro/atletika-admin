<!doctype html>
<html class="no-js" lang="">

    <?php include ('inc/head.inc.php'); ?>

    <body>

        <div class="page">

            <?php include ('inc/header.inc.php'); ?>

            <section class="main">
                <div class="container">

                    <div class="heading">
                        <h1>ТАБЛИЦА QR КОДОВ/АРТИКУЛОВ</h1>
                        <div class="heading__text">Последнее обновление базы: 11.12.2022, 15:32</div>
                    </div>

                    <div class="table_responsive mb_40">
                        <table class="table">
                            <tr>
                                <th class="table_long text_left">Название</th>
                                <th class="text_uppercase">КОД ПРОДАВЦА</th>
                                <th class="text_uppercase">КОД ПОКУПАТЕЛЯ</th>
                                <th>#</th>
                            </tr>
                            <tr>
                                <td class="table_long">Чулки для беременных #5685</td>
                                <td><a href="#">1922835912JDSLGSDHFKFHE 12.12.2018, 11:21:11</a></td>
                                <td>202283591JDSLGSDHFKFHE</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td class="table_long">Матрасы противопролежневые</td>
                                <td><a href="#">1922835912JDSLGSDHFKFHE 12.12.2018, 11:21:11</a></td>
                                <td>202283591JDSLGSDHFKFHE</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td class="table_long">Компрессионный трикотаж</td>
                                <td><a href="#">1922835912JDSLGSDHFKFHE 12.12.2018, 11:21:11</a></td>
                                <td>202283591JDSLGSDHFKFHE</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td class="table_long">Чулки для беременных #5685</td>
                                <td><a href="#">1922835912JDSLGSDHFKFHE 12.12.2018, 11:21:11</a></td>
                                <td>202283591JDSLGSDHFKFHE</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td class="table_long">Матрасы противопролежневые</td>
                                <td><a href="#">1922835912JDSLGSDHFKFHE 12.12.2018, 11:21:11</a></td>
                                <td>202283591JDSLGSDHFKFHE</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>Компрессионный трикотаж</td>
                                <td><a href="#">1922835912JDSLGSDHFKFHE 12.12.2018, 11:21:11</a></td>
                                <td>202283591JDSLGSDHFKFHE</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td class="table_long">Чулки для беременных #5685</td>
                                <td><a href="#">1922835912JDSLGSDHFKFHE 12.12.2018, 11:21:11</a></td>
                                <td>202283591JDSLGSDHFKFHE</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td class="table_long">Матрасы противопролежневые</td>
                                <td><a href="#">1922835912JDSLGSDHFKFHE 12.12.2018, 11:21:11</a></td>
                                <td>202283591JDSLGSDHFKFHE</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td class="table_long">Компрессионный трикотаж</td>
                                <td><a href="#">1922835912JDSLGSDHFKFHE 12.12.2018, 11:21:11</a></td>
                                <td>202283591JDSLGSDHFKFHE</td>
                                <td></td>
                            </tr>

                            <tr>
                                <td class="table_long">Матрасы противопролежневые</td>
                                <td><a href="#">1922835912JDSLGSDHFKFHE 12.12.2018, 11:21:11</a></td>
                                <td>202283591JDSLGSDHFKFHE</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>Компрессионный трикотаж</td>
                                <td><a href="#">1922835912JDSLGSDHFKFHE 12.12.2018, 11:21:11</a></td>
                                <td>202283591JDSLGSDHFKFHE</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td class="table_long">Чулки для беременных #5685</td>
                                <td><a href="#">1922835912JDSLGSDHFKFHE 12.12.2018, 11:21:11</a></td>
                                <td>202283591JDSLGSDHFKFHE</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td class="table_long">Матрасы противопролежневые</td>
                                <td><a href="#">1922835912JDSLGSDHFKFHE 12.12.2018, 11:21:11</a></td>
                                <td>202283591JDSLGSDHFKFHE</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td class="table_long">Компрессионный трикотаж</td>
                                <td><a href="#">1922835912JDSLGSDHFKFHE 12.12.2018, 11:21:11</a></td>
                                <td>202283591JDSLGSDHFKFHE</td>
                                <td></td>
                            </tr>
                        </table>
                    </div>

                    <ul class="pagination">
                        <li class="disable"><span><i class="fas fa-angle-left"></i></span></li>
                        <li class="active"><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">...</a></li>
                        <li><a href="#">15</a></li>
                        <li><a href="#">16</a></li>
                        <li><a href="#"><i class="fas fa-angle-right"></i></a></li>
                    </ul>

                </div>
            </section>

            <?php include ('inc/footer.inc.php'); ?>

        </div>

        <?php include ('inc/scripts.inc.php'); ?>

    </body>
</html>
