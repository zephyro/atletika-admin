<!doctype html>
<html class="no-js" lang="">

    <?php include ('inc/head.inc.php'); ?>

    <body>

        <div class="page">

            <?php include ('inc/header.inc.php'); ?>

            <section class="main">
                <div class="container">

                    <div class="heading">

                        <ul class="heading__nav">
                            <li class="active"><a href="#">Текущие баллы</a></li>
                            <li><a href="#">Сгенерировать подарочные карты</a></li>
                            <li><a href="#">История начисления</a></li>
                        </ul>

                        <h1>БАЛЛЫ ПРОДАВЦОВ</h1>
                    </div>

                    <div class="table_responsive mb_40">
                        <table class="table">
                            <tr>
                                <th class="table_long text_left">ПРОДАВЕЦ</th>
                                <th class="text_nowrap">БАЛЛОВ</th>
                                <th class="text_nowrap">РУБЛЕЙ</th>
                            </tr>
                            <tr>
                                <td class="table_long"><a href="#">Мария Петровна</a></td>
                                <td class="text_center">100</td>
                                <td class="text_center">50</td>
                            </tr>
                            <tr>
                                <td class="table_long"><a href="#">Мария Петровна</a></td>
                                <td class="text_center">100</td>
                                <td class="text_center">50</td>
                            </tr>
                            <tr>
                                <td class="table_long"><a href="#">Мария Петровна</a></td>
                                <td class="text_center">100</td>
                                <td class="text_center">50</td>
                            </tr>
                            <tr>
                                <td class="table_long"><a href="#">Мария Петровна</a></td>
                                <td class="text_center">100</td>
                                <td class="text_center">50</td>
                            </tr>
                            <tr>
                                <td class="table_long"><a href="#">Мария Петровна</a></td>
                                <td class="text_center">100</td>
                                <td class="text_center">50</td>
                            </tr>
                            <tr>
                                <td class="table_long"><a href="#">Мария Петровна</a></td>
                                <td class="text_center">100</td>
                                <td class="text_center">50</td>
                            </tr>
                            <tr>
                                <td class="table_long"><a href="#">Мария Петровна</a></td>
                                <td class="text_center">100</td>
                                <td class="text_center">50</td>
                            </tr>
                            <tr>
                                <td class="table_long"><a href="#">Мария Петровна</a></td>
                                <td class="text_center">100</td>
                                <td class="text_center">50</td>
                            </tr>
                            <tr>
                                <td class="table_long"><a href="#">Мария Петровна</a></td>
                                <td class="text_center">100</td>
                                <td class="text_center">50</td>
                            </tr>
                            <tr>
                                <td class="table_long"><a href="#">Мария Петровна</a></td>
                                <td class="text_center">100</td>
                                <td class="text_center">50</td>
                            </tr>
                            <tr>
                                <td class="table_long"><a href="#">Мария Петровна</a></td>
                                <td class="text_center">100</td>
                                <td class="text_center">50</td>
                            </tr>
                            <tr>
                                <td class="table_long"><a href="#">Мария Петровна</a></td>
                                <td class="text_center">100</td>
                                <td class="text_center">50</td>
                            </tr>
                            <tr>
                                <td class="table_long"><a href="#">Мария Петровна</a></td>
                                <td class="text_center">100</td>
                                <td class="text_center">50</td>
                            </tr>
                            <tr>
                                <td class="table_long"><a href="#">Мария Петровна</a></td>
                                <td class="text_center">100</td>
                                <td class="text_center">50</td>
                            </tr>
                            <tr>
                                <td class="table_long"><a href="#">Мария Петровна</a></td>
                                <td class="text_center">100</td>
                                <td class="text_center">50</td>
                            </tr>
                            <tr>
                                <td class="table_long"><a href="#">Мария Петровна</a></td>
                                <td class="text_center">100</td>
                                <td class="text_center">50</td>
                            </tr>
                            <tr>
                                <td class="table_long"><a href="#">Мария Петровна</a></td>
                                <td class="text_center">100</td>
                                <td class="text_center">50</td>
                            </tr>


                        </table>
                    </div>

                    <div class="form">
                        <div class="inline form_group">
                            <div class="inline__left">
                                <label class="form_label_sm">Тема письма</label>
                            </div>
                            <div class="inline__right">
                                <input type="text" class="form_control" name="theme" placeholder="" value="Мы приготовили для Вас подарочный купон">>
                            </div>
                        </div>
                        <div class="inline form_group">
                            <div class="inline__left">
                                <label class="form_label_sm">Написать сообщение для продавцов (отправлено будет E-mail)</label>
                            </div>
                            <div class="inline__right">
                                <textarea class="form_control" name="message" placeholder="" rows="8"></textarea>
                            </div>
                        </div>
                        <ul class="btn_group btn_group_right">
                            <li>
                                <button type="button" class="btn_text">Отправить на генерацию купонов</button>
                            </li>
                            <li>
                                <button type="submit" class="btn btn_double">Отправить E-mail и<br/>сгенерировать купоны</button>
                            </li>
                        </ul>
                    </div>

                </div>
            </section>

            <?php include ('inc/footer.inc.php'); ?>

        </div>

        <?php include ('inc/scripts.inc.php'); ?>

    </body>
</html>
