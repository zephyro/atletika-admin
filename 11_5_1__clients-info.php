<!doctype html>
<html class="no-js" lang="">

    <?php include ('inc/head.inc.php'); ?>

    <body>

        <div class="page">

            <?php include ('inc/header.inc.php'); ?>

            <section class="main">
                <div class="container">

                    <div class="heading">
                        <div class="heading__subtitle">Клиент | аптечаная сеть</div>
                        <div class="heading__row">
                            <div class="heading__col">
                                <h1 class="text_lowercase">Alex@gmail.com</h1>
                            </div>
                        </div>
                    </div>

                    <div class="profile">

                        <div class="profile__nav">
                            <ul>
                                <li class="active"><a href="#"><span>Продавцы сети</span></a></li>
                                <li><a href="#"><span>Общая информация</span></a></li>
                            </ul>
                        </div>

                        <div class="profile__content">

                            <div class="profile__heading">Провизоры аптечной сети</div>

                            <div class="table_responsive">

                                <table class="table_light">
                                    <tr>
                                        <th>Имя</th>
                                        <th>Дата регистрации</th>
                                        <th>Адрес аптеки</th>
                                        <th>Сколько продали</th>
                                    </tr>
                                    <tr>
                                        <td><a href="#">Алексей</a></td>
                                        <td class="text_nowrap">04.12.2017, 11:43</td>
                                        <td>Москва ул. Куйбышева 32 </td>
                                        <td>35</td>
                                    </tr>
                                    <tr>
                                        <td><a href="#">Алексей</a></td>
                                        <td class="text_nowrap">04.12.2017, 11:43</td>
                                        <td>Москва ул. Куйбышева 32 </td>
                                        <td>35</td>
                                    </tr>
                                    <tr>
                                        <td><a href="#">Алексей</a></td>
                                        <td class="text_nowrap">04.12.2017, 11:43</td>
                                        <td>Москва ул. Куйбышева 32 </td>
                                        <td>35</td>
                                    </tr>
                                    <tr>
                                        <td><a href="#">Алексей</a></td>
                                        <td class="text_nowrap">04.12.2017, 11:43</td>
                                        <td>Москва ул. Куйбышева 32 </td>
                                        <td>35</td>
                                    </tr>
                                    <tr>
                                        <td><a href="#">Алексей</a></td>
                                        <td class="text_nowrap">04.12.2017, 11:43</td>
                                        <td>Москва ул. Куйбышева 32 </td>
                                        <td>35</td>
                                    </tr>
                                    <tr>
                                        <td><a href="#">Алексей</a></td>
                                        <td class="text_nowrap">04.12.2017, 11:43</td>
                                        <td>Москва ул. Куйбышева 32 </td>
                                        <td>35</td>
                                    </tr>
                                </table>

                            </div>
                        </div>

                    </div>

                </div>
            </section>

            <?php include ('inc/footer.inc.php'); ?>

        </div>

        <?php include ('inc/scripts.inc.php'); ?>

    </body>
</html>
