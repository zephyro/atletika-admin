
$(".btn_modal").fancybox({
    'padding'    : 0
});

// SVG IE11 support
svg4everybody();

// Nav
(function() {

    $('.nav_toggle').on('click touchstart', function(e){
        e.preventDefault();

        $(this).toggleClass('open');
        $('.header__nav').toggleClass('open');
    });

}());

// select

$('.select__active').on('click touchstart', function(e){
    e.preventDefault();

    if($(this).closest('.select').hasClass('open')){
        $(this).closest('.select').toggleClass('open');
    }
    else {
        $('.select').removeClass('open');
        $(this).closest('.select').toggleClass('open');
    }
});


$('.select__dropdown li a').on('click touchstart', function(e){

    $(this).closest('.select').removeClass('open');
});


// Hide dropdown

$('body').click(function (event) {

    if ($(event.target).closest(".select").length === 0) {
        $(".select").removeClass('open');
    }

});