<!doctype html>
<html class="no-js" lang="">

    <?php include ('inc/head.inc.php'); ?>

    <body>

        <div class="page">

            <?php include ('inc/header.inc.php'); ?>

            <section class="main">
                <div class="container">

                    <div class="heading">
                        <div class="heading__subtitle">Покупатель</div>
                        <div class="heading__row">
                            <div class="heading__col">
                                <h1><span class="text_lowercase">Alex@gmail.com</span></h1>
                            </div>
                        </div>
                    </div>

                    <div class="profile">

                        <div class="profile__nav">
                            <ul>
                                <li><a href="#"><span>Инфо</span></a></li>
                                <li><a href="#"><span>Зачисление/списание баллов</span></a></li>
                                <li class="active"><a href="#"><span>История входа</span></a></li>
                            </ul>
                        </div>

                        <div class="profile__content">


                            <div class="profile__heading">История входа пользователя</div>

                            <ul class="login_history">
                                <li>15.122.11.222   |  11.04.2018, 11:34  |  156.11.23.29 </li>
                                <li>15.122.11.222   |  11.04.2018, 11:34  |  156.11.23.29 </li>
                                <li>15.122.11.222   |  11.04.2018, 11:34  |  156.11.23.29 </li>
                                <li>15.122.11.222   |  11.04.2018, 11:34  |  156.11.23.29 </li>
                                <li>15.122.11.222   |  11.04.2018, 11:34  |  156.11.23.29 </li>
                                <li>15.122.11.222   |  11.04.2018, 11:34  |  156.11.23.29 </li>
                            </ul>


                        </div>

                    </div>

                </div>
            </section>

            <?php include ('inc/footer.inc.php'); ?>

        </div>

        <?php include ('inc/scripts.inc.php'); ?>

    </body>
</html>
