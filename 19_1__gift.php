<!doctype html>
<html class="no-js" lang="">

    <?php include ('inc/head.inc.php'); ?>

    <body>

        <div class="page">

            <?php include ('inc/header.inc.php'); ?>

            <section class="main">
                <div class="container">

                    <div class="heading">
                        <ul class="heading__nav">
                            <li class="active"><a href="#">Текущие баллы</a></li>
                            <li><a href="#">Сделанные списание баллов</a></li>
                        </ul>

                        <h1>ПОДАРКИ ПОКУПАТЕЛЯМ</h1>
                        <div class="heading__text">
                            <div class="mb_10">Общее количество текущих  баллов  = 250</div>
                            <div>Общее количество текущих  баллов  = 250</div>
                        </div>
                    </div>

                    <div class="blue_box">
                        <ul class="form_row form_row_reverse">
                            <li>
                                <div class="form_row__label">Найти всех покупателей у которых больше</div>
                            </li>
                            <li>
                                <div class="form_inline">
                                    <input type="text" class="form_control_sm" name="name" value="80" placeholder="" style="width: 100px;">
                                    <span>баллов и</span>
                                </div>
                            </li>

                            <li>
                                <button type="submit" class="btn">Отправить им подарки</button>
                            </li>
                        </ul>
                    </div>
                    <div class="table_responsive mb_40">
                        <table class="table">
                            <tr>
                                <th class="table_long text_left">Покупатель</th>
                                <th class="text_nowrap">Текущее кол-во</th>
                                <th class="text_nowrap">За последний год</th>
                                <th class="text_nowrap">Всего баллов</th>
                                <th>#</th>
                            </tr>
                            <tr>
                                <td class="table_long"><a href="#">Мария Петровна</a></td>
                                <td class="text_center">100</td>
                                <td class="text_center">50</td>
                                <td class="text_center">25000</td>
                                <td>
                                    <a class="btn_play" href="">
                                        <img src="assets/img/icon__play.svg" class="img-fluid" alt="">
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td class="table_long"><a href="#">Мария Петровна</a></td>
                                <td class="text_center">100</td>
                                <td class="text_center">50</td>
                                <td class="text_center">25000</td>
                                <td>
                                    <a class="btn_play" href="">
                                        <img src="assets/img/icon__play.svg" class="img-fluid" alt="">
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td class="table_long"><a href="#">Мария Петровна</a></td>
                                <td class="text_center">100</td>
                                <td class="text_center">50</td>
                                <td class="text_center">25000</td>
                                <td>
                                    <a class="btn_play" href="">
                                        <img src="assets/img/icon__play.svg" class="img-fluid" alt="">
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td class="table_long"><a href="#">Мария Петровна</a></td>
                                <td class="text_center">100</td>
                                <td class="text_center">50</td>
                                <td class="text_center">25000</td>
                                <td>
                                    <a class="btn_play" href="">
                                        <img src="assets/img/icon__play.svg" class="img-fluid" alt="">
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td class="table_long"><a href="#">Мария Петровна</a></td>
                                <td class="text_center">100</td>
                                <td class="text_center">50</td>
                                <td class="text_center">25000</td>
                                <td>
                                    <a class="btn_play" href="">
                                        <img src="assets/img/icon__play.svg" class="img-fluid" alt="">
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td class="table_long"><a href="#">Мария Петровна</a></td>
                                <td class="text_center">100</td>
                                <td class="text_center">50</td>
                                <td class="text_center">25000</td>
                                <td>
                                    <a class="btn_play" href="">
                                        <img src="assets/img/icon__play.svg" class="img-fluid" alt="">
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td class="table_long"><a href="#">Мария Петровна</a></td>
                                <td class="text_center">100</td>
                                <td class="text_center">50</td>
                                <td class="text_center">25000</td>
                                <td>
                                    <a class="btn_play" href="">
                                        <img src="assets/img/icon__play.svg" class="img-fluid" alt="">
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td class="table_long"><a href="#">Мария Петровна</a></td>
                                <td class="text_center">100</td>
                                <td class="text_center">50</td>
                                <td class="text_center">25000</td>
                                <td>
                                    <a class="btn_play" href="">
                                        <img src="assets/img/icon__play.svg" class="img-fluid" alt="">
                                    </a>
                                </td>
                            </tr>
                        </table>
                    </div>

                    <ul class="pagination">
                        <li class="disable"><span><i class="fas fa-angle-left"></i></span></li>
                        <li class="active"><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">...</a></li>
                        <li><a href="#">15</a></li>
                        <li><a href="#">16</a></li>
                        <li><a href="#"><i class="fas fa-angle-right"></i></a></li>
                    </ul>


                </div>
            </section>

            <?php include ('inc/footer.inc.php'); ?>

        </div>

        <?php include ('inc/scripts.inc.php'); ?>

    </body>
</html>
