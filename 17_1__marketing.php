<!doctype html>
<html class="no-js" lang="">

    <?php include ('inc/head.inc.php'); ?>

    <body>

        <div class="page">

            <?php include ('inc/header.inc.php'); ?>

            <section class="main">
                <div class="container">

                    <div class="heading">
                        <ul class="heading__nav">
                            <li class="active"><a href="#">Рассылки E-mail</a></li>
                            <li><a href="#">Рассылки SMS</a></li>
                        </ul>

                        <div class="heading__row">
                            <div class="heading__col">
                                <h1>Рассылки E-mail</h1>
                            </div>
                            <div class="heading__col">
                                <a href="#" class="btn">Создать E-MAIL рассылку</a>
                            </div>
                        </div>
                    </div>

                    <div class="table_responsive mb_40">
                        <table class="table">
                            <tr>
                                <th class="table_long text_left">Название</th>
                                <th>#</th>
                            </tr>
                            <tr>
                                <td class="table_long"><a href="#">Мария Петровна</a></td>
                                <td>
                                    <a class="btn_play" href="">
                                        <img src="assets/img/icon__play.svg" class="img-fluid" alt="">
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td class="table_long"><a href="#">Мария Петровна</a></td>
                                <td>
                                    <a class="btn_play" href="">
                                        <img src="assets/img/icon__play.svg" class="img-fluid" alt="">
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td class="table_long"><a href="#">Мария Петровна</a></td>
                                <td>
                                    <a class="btn_play" href="">
                                        <img src="assets/img/icon__play.svg" class="img-fluid" alt="">
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td class="table_long"><a href="#">Мария Петровна</a></td>
                                <td>
                                    <a class="btn_play" href="">
                                        <img src="assets/img/icon__play.svg" class="img-fluid" alt="">
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td class="table_long"><a href="#">Мария Петровна</a></td>
                                <td>
                                    <a class="btn_play" href="">
                                        <img src="assets/img/icon__play.svg" class="img-fluid" alt="">
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td class="table_long"><a href="#">Мария Петровна</a></td>
                                <td>
                                    <a class="btn_play" href="">
                                        <img src="assets/img/icon__play.svg" class="img-fluid" alt="">
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td class="table_long"><a href="#">Мария Петровна</a></td>
                                <td>
                                    <a class="btn_play" href="">
                                        <img src="assets/img/icon__play.svg" class="img-fluid" alt="">
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td class="table_long"><a href="#">Мария Петровна</a></td>
                                <td>
                                    <a class="btn_play" href="">
                                        <img src="assets/img/icon__play.svg" class="img-fluid" alt="">
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td class="table_long"><a href="#">Мария Петровна</a></td>
                                <td>
                                    <a class="btn_play" href="">
                                        <img src="assets/img/icon__play.svg" class="img-fluid" alt="">
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td class="table_long"><a href="#">Мария Петровна</a></td>
                                <td>
                                    <a class="btn_play" href="">
                                        <img src="assets/img/icon__play.svg" class="img-fluid" alt="">
                                    </a>
                                </td>
                            </tr>
                        </table>
                    </div>

                    <ul class="pagination">
                        <li class="disable"><span><i class="fas fa-angle-left"></i></span></li>
                        <li class="active"><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">...</a></li>
                        <li><a href="#">15</a></li>
                        <li><a href="#">16</a></li>
                        <li><a href="#"><i class="fas fa-angle-right"></i></a></li>
                    </ul>

                </div>
            </section>

            <?php include ('inc/footer.inc.php'); ?>

        </div>

        <?php include ('inc/scripts.inc.php'); ?>

    </body>
</html>
