<!doctype html>
<html class="no-js" lang="">

    <?php include ('inc/head.inc.php'); ?>

    <body>

        <div class="page">

            <?php include ('inc/header.inc.php'); ?>

            <section class="main">
                <div class="container">

                    <div class="heading">
                        <h1>ОБЩИЕ НАСТРОЙКИ ДЛЯ ВСЕХ</h1>
                    </div>

                    <div class="profile">

                        <div class="profile__nav">
                            <ul>
                                <li class="active"><a href="#"><span>Уведомления первостольникам</span></a></li>
                                <li><a href="#"><span>Уведомления покупателям</span></a></li>
                                <li><a href="#"><span>Настройка баллов</span></a></li>
                                <li><a href="#"><span>Коды товарной группы</span></a></li>
                            </ul>
                        </div>

                        <div class="profile__content">

                            <div class="profile__heading">Уведомления для ПРОДАВЦОВ</div>

                            <div class="inline form_group">
                                <div class="inline__left">
                                    <label class="form_label">Для провизоров</label>
                                </div>
                                <div class="inline__right">
                                    <textarea class="form_control" name="message" placeholder="Только текст" rows="8"></textarea>
                                </div>
                            </div>

                            <div class="inline form_group">
                                <div class="inline__left">

                                </div>
                                <div class="inline__right">
                                    <button type="submit" class="btn">Сохранить</button>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>
            </section>

            <?php include ('inc/footer.inc.php'); ?>

        </div>

        <?php include ('inc/scripts.inc.php'); ?>

    </body>
</html>
