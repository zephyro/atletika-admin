<!doctype html>
<html class="no-js" lang="">

    <?php include ('inc/head.inc.php'); ?>

    <body>

        <div class="page">

            <?php include ('inc/header.inc.php'); ?>

            <section class="main">
                <div class="container">

                    <div class="heading">

                        <ul class="heading__nav">
                            <li><a href="#">Текущие баллы</a></li>
                            <li class="active"><a href="#">Сгенерировать подарочные карты</a></li>
                            <li><a href="#">История начисления</a></li>
                        </ul>
                        <div class="heading__subtitle color_gray">Генерация подарочных сертификатов для провизоров</div>
                        <h1>СГЕНЕРИРОВАТЬ ПОДАРОЧНЫЕ КАРТЫ</h1>
                    </div>

                    <div class="form_block">
                        <div class="inline form_group">
                            <div class="inline__left inline__left_lg">
                                <label class="form_label_sm">Название списка подарочных карт</label>
                            </div>
                            <div class="inline__right">
                                <input type="text" class="form_control" name="email" placeholder="" value="alex928@gmail.com">
                            </div>
                        </div>
                        <div class="inline form_group">
                            <div class="inline__left inline__left_lg">
                                <label class="form_label_sm">Дата подарочных карт</label>
                            </div>
                            <div class="inline__right">
                                <div class="mw_150">
                                    <select class="form_control form_select" name="select">
                                        <option value="">15.11.2018</option>
                                        <option value="">20.11.2018</option>
                                        <option value="">25.11.2018</option>
                                        <option value="">30.11.2018</option>
                                        <option value="">05.12.2018</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="inline form_group">
                            <div class="inline__left inline__left_lg">
                                <label class="form_label_sm">Количество баллов для покупателей</label>
                            </div>
                            <div class="inline__right">
                                <div class="mw_100">
                                    <input type="text" class="form_control form_control_blue" name="email" placeholder="" value="10000">
                                </div>
                            </div>
                        </div>
                        <div class="inline form_group">
                            <div class="inline__left inline__left_lg">
                                <label class="form_label_sm">Количество рублей</label>
                            </div>
                            <div class="inline__right">
                                <div class="mw_100">
                                    <input type="text" class="form_control form_control_blue" name="email" placeholder="" value="10000">
                                </div>
                            </div>
                        </div>
                        <div class="inline">
                            <div class="inline__left inline__left_lg">
                            </div>
                            <div class="inline__right">
                                <button class="btn" type="submit">СГЕНЕРИРОВАТЬ КОДЫ ДЛЯ ПЕЧАТИ</button>
                            </div>
                        </div>
                    </div>

                </div>
            </section>

            <?php include ('inc/footer.inc.php'); ?>

        </div>

        <?php include ('inc/scripts.inc.php'); ?>

    </body>
</html>
