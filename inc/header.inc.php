<header class="header">
    <div class="container">
        <div class="header__row">
            <a href="/" class="header__logo">
                <i><img src="assets/img/logo.svg" class="img-fluid" alt=""></i>
                <span>Решает, украшает, привлекает!</span>
            </a>

            <nav class="header__nav">
                <ul>
                    <li><a href="#"><span>Подарки покупателям</span></a></li>
                    <li><a href="#"><span>Переводы</span></a></li>
                    <li><a href="#"><span>Аптеки</span></a></li>
                    <li><a href="#"><span>Пользователи</span></a></li>
                    <li><a href="#"><span>Маркетинг</span></a></li>
                    <li><a href="#"><span>Подарочные карты</span></a></li>
                    <li><a href="#"><span>QR КОДЫ</span></a></li>
                    <li><a href="#"><span>Тексты</span></a></li>
                    <li><a href="#"><span>Настройки</span></a></li>
                </ul>
            </nav>

            <a class="header__toggle nav_toggle" href="#">
                <span></span>
                <span></span>
                <span></span>
            </a>

        </div>
    </div>
</header>