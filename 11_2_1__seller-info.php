<!doctype html>
<html class="no-js" lang="">

    <?php include ('inc/head.inc.php'); ?>

    <body>

        <div class="page">

            <?php include ('inc/header.inc.php'); ?>

            <section class="main">
                <div class="container">

                    <div class="heading">
                        <div class="heading__subtitle">Продавец</div>
                        <div class="heading__row">
                            <div class="heading__col">
                                <h1><a href="#" class="text_lowercase text_lowercase color_red">Alex@gmail.com</a></h1>
                            </div>
                            <div class="heading__col">
                                <div class="select">
                                    <div class="select__active">
                                        <span>Выберите действие</span>
                                    </div>
                                    <ul class="select__dropdown">
                                        <li><a href="#">Разрешить работу</a></li>
                                        <li><a href="#">Запретить работу</a></li>


                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="profile">

                        <div class="profile__nav">
                            <ul>
                                <li class="active"><a href="#"><span>Инфо</span></a></li>
                                <li><a href="#"><span>Зачисление/списание баллов</span></a></li>
                                <li><a href="#"><span>Подарочные карты</span></a></li>
                                <li><a href="#"><span>История входа</span></a></li>
                            </ul>
                        </div>

                        <div class="profile__content">


                            <div class="profile__heading">Сводная информация о ПРОДАВЦЕ</div>

                            <div class="profile__content_wrap">


                                <div class="profile__group">
                                    <div class="inline">
                                        <div class="inline__left"></div>
                                        <div class="inline__right">
                                            <h3>О ПРОДАВЦЕ</h3>
                                        </div>
                                    </div>
                                    <div class="profile__about">
                                        <div class="inline">
                                            <div class="inline__left"><strong>Тип</strong></div>
                                            <div class="inline__right">Продавец</div>
                                        </div>
                                        <div class="inline">
                                            <div class="inline__left"><strong>Язык пользователя</strong></div>
                                            <div class="inline__right">RU</div>
                                        </div>
                                        <div class="inline">
                                            <div class="inline__left"><strong>Дата регистрации</strong></div>
                                            <div class="inline__right">15.11.2018; 11:43;</div>
                                        </div>
                                        <div class="inline">
                                            <div class="inline__left"><strong>Последний вход</strong></div>
                                            <div class="inline__right">15.11.2018; 11:43;</div>
                                        </div>
                                    </div>
                                </div>

                                <div class="profile__group mb_40">
                                    <div class="inline">
                                        <div class="inline__left"></div>
                                        <div class="inline__right">
                                            <h3>ИНФОРМАЦИЯ О ПРОДАВЦЕ</h3>
                                        </div>
                                    </div>

                                    <div class="inline form_group">
                                        <div class="inline__left">
                                            <label class="form_label">e-MAIL</label>
                                        </div>
                                        <div class="inline__right">
                                            <input type="text" class="form_control" name="email" placeholder="" value="alex928@gmail.com">
                                        </div>
                                    </div>

                                    <div class="inline form_group">
                                        <div class="inline__left">
                                            <label class="form_label">Имя</label>
                                        </div>
                                        <div class="inline__right">
                                            <input type="text" class="form_control" name="email" placeholder="" value="Алексей">
                                        </div>
                                    </div>

                                    <div class="inline form_group">
                                        <div class="inline__left">
                                            <label class="form_label">Телефон</label>
                                        </div>
                                        <div class="inline__right">
                                            <input type="text" class="form_control" name="phone" placeholder="" value="+7927555555">
                                        </div>
                                    </div>

                                    <div class="inline form_group">
                                        <div class="inline__left">
                                            <label class="form_label">Аптечная сеть</label>
                                        </div>
                                        <div class="inline__right">
                                            <select class="form_control form_select" name="select">
                                                <option value="">Аптечная сеть</option>
                                                <option value="">Аптечная сеть</option>
                                                <option value="">Аптечная сеть</option>
                                                <option value="">Аптечная сеть</option>
                                                <option value="">Аптечная сеть</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="inline form_group">
                                        <div class="inline__left">
                                            <label class="form_label">Адрес аптеки</label>
                                        </div>
                                        <div class="inline__right">
                                            <input type="text" class="form_control" name="address" placeholder="" value="Алексей">
                                        </div>
                                    </div>

                                    <div class="inline">
                                        <div class="inline__left"></div>
                                        <div class="inline__right">
                                            <button type="submit" class="btn btn__rounded">Сохранить информацию</button>
                                        </div>
                                    </div>

                                </div>

                                <div class="profile__group">
                                    <div class="inline">
                                        <div class="inline__left"></div>
                                        <div class="inline__right"><h3>Новый пароль</h3></div>
                                    </div>

                                    <div class="inline">
                                        <div class="inline__left"></div>
                                        <div class="inline__right"></div>
                                    </div>
                                    <div class="inline form_group">
                                        <div class="inline__left">
                                            <label class="form_label">нОВЫЙ ПАРОЛЬ</label>
                                        </div>
                                        <div class="inline__right">
                                            <input type="password" class="form_control" name="email" placeholder="" value="">
                                        </div>
                                    </div>
                                    <div class="inline form_group">
                                        <div class="inline__left">
                                            <label class="form_label">ПОВТОРИТЬ ПАРОЛЬ</label>
                                        </div>
                                        <div class="inline__right">
                                            <input type="password" class="form_control" name="email" placeholder="" value="">
                                        </div>
                                    </div>

                                    <div class="inline">
                                        <div class="inline__left"></div>
                                        <div class="inline__right">
                                            <button type="submit" class="btn btn__rounded">Сохранить новый пароль</button>
                                        </div>
                                    </div>
                                </div>




                            </div>

                        </div>

                    </div>

                </div>
            </section>

            <?php include ('inc/footer.inc.php'); ?>

        </div>

        <?php include ('inc/scripts.inc.php'); ?>

    </body>
</html>
