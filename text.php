<!doctype html>
<html class="no-js" lang="">

    <?php include ('inc/head.inc.php'); ?>

    <body>

        <div class="page">

            <?php include ('inc/header.inc.php'); ?>

            <section class="main">
                <div class="container">

                    <div class="heading">

                        <ul class="heading__nav">
                            <li class="active"><a href="#">Страницы</a></li>
                            <li><a href="#">Новости для покупателей</a></li>
                            <li><a href="#">Новости для первостольников</a></li>
                            <li><a href="#">Статьи 10 баллов за прочтение</a></li>
                        </ul>

                        <h1>ВСЕ ТЕКСТЫ</h1>
                    </div>

                    <div class="table_responsive mb_40">
                        <table class="table">
                            <tr>
                                <th class="table_long text_left">Название</th>
                                <th>Дата создания</th>
                                <th>Тип</th>
                                <th>#</th>
                            </tr>
                            <tr>
                                <td class="table_long"><a href="#">Компрессионный трикотаж | https://site.name/stranica1</a></td>
                                <td class="text_nowrap">15.11.2018, 12:32</td>
                                <td class="text_nowrap">Страница</td>
                                <td>
                                    <a class="btn_play" href="">
                                        <img src="assets/img/icon__play.svg" class="img-fluid" alt="">
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td class="table_long"><a href="#">Компрессионный трикотаж | https://site.name/stranica1</a></td>
                                <td class="text_nowrap">15.11.2018, 12:32</td>
                                <td class="text_nowrap">Для продавцов</td>
                                <td>
                                    <a class="btn_play" href="">
                                        <img src="assets/img/icon__play.svg" class="img-fluid" alt="">
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td class="table_long"><a href="#">Компрессионный трикотаж | https://site.name/stranica1</a></td>
                                <td>15.11.2018, 12:32</td>
                                <td class="text_nowrap">Для покупателей</td>
                                <td class="text_nowrap">
                                    <a class="btn_play" href="">
                                        <img src="assets/img/icon__play.svg" class="img-fluid" alt="">
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td class="table_long"><a href="#">Компрессионный трикотаж | https://site.name/stranica1</a></td>
                                <td class="text_nowrap">15.11.2018, 12:32</td>
                                <td class="text_nowrap">
                                    Для покупателей<br/>
                                    Для продавцов
                                </td>
                                <td>
                                    <a class="btn_play" href="">
                                        <img src="assets/img/icon__play.svg" class="img-fluid" alt="">
                                    </a>
                                </td>
                            </tr>

                            <tr>
                                <td class="table_long"><a href="#">Компрессионный трикотаж | https://site.name/stranica1</a></td>
                                <td class="text_nowrap">15.11.2018, 12:32</td>
                                <td class="text_nowrap">Страница</td>
                                <td>
                                    <a class="btn_play" href="">
                                        <img src="assets/img/icon__play.svg" class="img-fluid" alt="">
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td class="table_long"><a href="#">Компрессионный трикотаж | https://site.name/stranica1</a></td>
                                <td class="text_nowrap">15.11.2018, 12:32</td>
                                <td class="text_nowrap">Для продавцов</td>
                                <td>
                                    <a class="btn_play" href="">
                                        <img src="assets/img/icon__play.svg" class="img-fluid" alt="">
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td class="table_long"><a href="#">Компрессионный трикотаж | https://site.name/stranica1</a></td>
                                <td>15.11.2018, 12:32</td>
                                <td class="text_nowrap">Для покупателей</td>
                                <td class="text_nowrap">
                                    <a class="btn_play" href="">
                                        <img src="assets/img/icon__play.svg" class="img-fluid" alt="">
                                    </a>
                                </td>
                            </tr>

                            <tr>
                                <td class="table_long"><a href="#">Компрессионный трикотаж | https://site.name/stranica1</a></td>
                                <td class="text_nowrap">15.11.2018, 12:32</td>
                                <td class="text_nowrap">Страница</td>
                                <td>
                                    <a class="btn_play" href="">
                                        <img src="assets/img/icon__play.svg" class="img-fluid" alt="">
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td class="table_long"><a href="#">Компрессионный трикотаж | https://site.name/stranica1</a></td>
                                <td class="text_nowrap">15.11.2018, 12:32</td>
                                <td class="text_nowrap">Для продавцов</td>
                                <td>
                                    <a class="btn_play" href="">
                                        <img src="assets/img/icon__play.svg" class="img-fluid" alt="">
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td class="table_long"><a href="#">Компрессионный трикотаж | https://site.name/stranica1</a></td>
                                <td>15.11.2018, 12:32</td>
                                <td class="text_nowrap">Для покупателей</td>
                                <td class="text_nowrap">
                                    <a class="btn_play" href="">
                                        <img src="assets/img/icon__play.svg" class="img-fluid" alt="">
                                    </a>
                                </td>
                            </tr>

                            <tr>
                                <td class="table_long"><a href="#">Компрессионный трикотаж | https://site.name/stranica1</a></td>
                                <td class="text_nowrap">15.11.2018, 12:32</td>
                                <td class="text_nowrap">Страница</td>
                                <td>
                                    <a class="btn_play" href="">
                                        <img src="assets/img/icon__play.svg" class="img-fluid" alt="">
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td class="table_long"><a href="#">Компрессионный трикотаж | https://site.name/stranica1</a></td>
                                <td class="text_nowrap">15.11.2018, 12:32</td>
                                <td class="text_nowrap">Для продавцов</td>
                                <td>
                                    <a class="btn_play" href="">
                                        <img src="assets/img/icon__play.svg" class="img-fluid" alt="">
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td class="table_long"><a href="#">Компрессионный трикотаж | https://site.name/stranica1</a></td>
                                <td>15.11.2018, 12:32</td>
                                <td class="text_nowrap">Для покупателей</td>
                                <td class="text_nowrap">
                                    <a class="btn_play" href="">
                                        <img src="assets/img/icon__play.svg" class="img-fluid" alt="">
                                    </a>
                                </td>
                            </tr>

                            <tr>
                                <td class="table_long"><a href="#">Компрессионный трикотаж | https://site.name/stranica1</a></td>
                                <td class="text_nowrap">15.11.2018, 12:32</td>
                                <td class="text_nowrap">Страница</td>
                                <td>
                                    <a class="btn_play" href="">
                                        <img src="assets/img/icon__play.svg" class="img-fluid" alt="">
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td class="table_long"><a href="#">Компрессионный трикотаж | https://site.name/stranica1</a></td>
                                <td class="text_nowrap">15.11.2018, 12:32</td>
                                <td class="text_nowrap">Для продавцов</td>
                                <td>
                                    <a class="btn_play" href="">
                                        <img src="assets/img/icon__play.svg" class="img-fluid" alt="">
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td class="table_long"><a href="#">Компрессионный трикотаж | https://site.name/stranica1</a></td>
                                <td>15.11.2018, 12:32</td>
                                <td class="text_nowrap">Для покупателей</td>
                                <td class="text_nowrap">
                                    <a class="btn_play" href="">
                                        <img src="assets/img/icon__play.svg" class="img-fluid" alt="">
                                    </a>
                                </td>
                            </tr>

                            <tr>
                                <td class="table_long"><a href="#">Компрессионный трикотаж | https://site.name/stranica1</a></td>
                                <td class="text_nowrap">15.11.2018, 12:32</td>
                                <td class="text_nowrap">Страница</td>
                                <td>
                                    <a class="btn_play" href="">
                                        <img src="assets/img/icon__play.svg" class="img-fluid" alt="">
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td class="table_long"><a href="#">Компрессионный трикотаж | https://site.name/stranica1</a></td>
                                <td class="text_nowrap">15.11.2018, 12:32</td>
                                <td class="text_nowrap">Для продавцов</td>
                                <td>
                                    <a class="btn_play" href="">
                                        <img src="assets/img/icon__play.svg" class="img-fluid" alt="">
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td class="table_long"><a href="#">Компрессионный трикотаж | https://site.name/stranica1</a></td>
                                <td>15.11.2018, 12:32</td>
                                <td class="text_nowrap">Для покупателей</td>
                                <td class="text_nowrap">
                                    <a class="btn_play" href="">
                                        <img src="assets/img/icon__play.svg" class="img-fluid" alt="">
                                    </a>
                                </td>
                            </tr>

                            <tr>
                                <td class="table_long"><a href="#">Компрессионный трикотаж | https://site.name/stranica1</a></td>
                                <td class="text_nowrap">15.11.2018, 12:32</td>
                                <td class="text_nowrap">Страница</td>
                                <td>
                                    <a class="btn_play" href="">
                                        <img src="assets/img/icon__play.svg" class="img-fluid" alt="">
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td class="table_long"><a href="#">Компрессионный трикотаж | https://site.name/stranica1</a></td>
                                <td class="text_nowrap">15.11.2018, 12:32</td>
                                <td class="text_nowrap">Для продавцов</td>
                                <td>
                                    <a class="btn_play" href="">
                                        <img src="assets/img/icon__play.svg" class="img-fluid" alt="">
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td class="table_long"><a href="#">Компрессионный трикотаж | https://site.name/stranica1</a></td>
                                <td>15.11.2018, 12:32</td>
                                <td class="text_nowrap">Для покупателей</td>
                                <td class="text_nowrap">
                                    <a class="btn_play" href="">
                                        <img src="assets/img/icon__play.svg" class="img-fluid" alt="">
                                    </a>
                                </td>
                            </tr>

                        </table>
                    </div>

                    <ul class="pagination">
                        <li class="disable"><span><i class="fas fa-angle-left"></i></span></li>
                        <li class="active"><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">...</a></li>
                        <li><a href="#">15</a></li>
                        <li><a href="#">16</a></li>
                        <li><a href="#"><i class="fas fa-angle-right"></i></a></li>
                    </ul>

                </div>
            </section>

            <?php include ('inc/footer.inc.php'); ?>

        </div>

        <?php include ('inc/scripts.inc.php'); ?>

    </body>
</html>
