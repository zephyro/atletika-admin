<!doctype html>
<html class="no-js" lang="">

    <?php include ('inc/head.inc.php'); ?>

    <body>

        <div class="page">

            <?php include ('inc/header.inc.php'); ?>

            <section class="main">
                <div class="container">

                    <div class="heading">
                        <div class="heading__subtitle">Клиент | аптечаная сеть</div>
                        <div class="heading__row">
                            <div class="heading__col">
                                <h1 class="text_lowercase">Alex@gmail.com</h1>
                            </div>
                        </div>
                    </div>

                    <div class="profile">

                        <div class="profile__nav">
                            <ul>
                                <li><a href="#"><span>Продавцы сети</span></a></li>
                                <li class="active"><a href="#"><span>Изменить пароль</span></a></li>
                            </ul>
                        </div>

                        <div class="profile__content">

                            <div class="profile__heading">Изменить пароль</div>

                            <div class="profile__content_wrap">

                                <div class="profile__group">
                                    <div class="inline">
                                        <div class="inline__left"></div>
                                        <div class="inline__right"><h3>Новый пароль</h3></div>
                                    </div>

                                    <div class="inline form_group">
                                        <div class="inline__left">
                                            <label class="form_label">нОВЫЙ ПАРОЛЬ</label>
                                        </div>
                                        <div class="inline__right">
                                            <input type="password" class="form_control" name="email" placeholder="" value="">
                                        </div>
                                    </div>
                                    <div class="inline form_group">
                                        <div class="inline__left">
                                            <label class="form_label">ПОВТОРИТЬ ПАРОЛЬ</label>
                                        </div>
                                        <div class="inline__right">
                                            <input type="password" class="form_control" name="email" placeholder="" value="">
                                        </div>
                                    </div>

                                    <div class="inline">
                                        <div class="inline__left"></div>
                                        <div class="inline__right">
                                            <button type="submit" class="btn btn__rounded">Сохранить новый пароль</button>
                                        </div>
                                    </div>
                                </div>

                            </div>


                        </div>

                    </div>

                </div>
            </section>

            <?php include ('inc/footer.inc.php'); ?>

        </div>

        <?php include ('inc/scripts.inc.php'); ?>

    </body>
</html>
