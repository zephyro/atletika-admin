<!doctype html>
<html class="no-js" lang="">

    <?php include ('inc/head.inc.php'); ?>

    <body>

        <div class="page">

            <?php include ('inc/header.inc.php'); ?>

            <section class="main">
                <div class="container">

                    <div class="heading">

                        <ul class="heading__nav">
                            <li><a href="#">Все</a></li>
                            <li class="active"><a href="#">Покупатели</a></li>
                            <li><a href="#">Продавцы</a></li>
                            <li><a href="#">Клиенты</a></li>
                        </ul>

                        <h1>покупатели</h1>
                    </div>


                    <div class="table_responsive mb_40">
                        <table class="table">
                            <tr>
                                <th>#</th>
                                <th class="table_long text_left text_uppercase">E-MAIL</th>
                                <th>Баллов</th>
                                <th>Дата регистрации</th>
                                <th>#</th>
                            </tr>
                            <tr>
                                <td>001</td>
                                <td class="table_long"><a href="#">75@ya.ru</a></td>
                                <td>155</td>
                                <td class="text_nowrap">15.11.2018, 12:32</td>
                                <td>
                                    <a class="btn_play" href="">
                                        <img src="assets/img/icon__play.svg" class="img-fluid" alt="">
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td>002</td>
                                <td class="table_long"><a href="#">75@ya.ru</a></td>
                                <td>155</td>
                                <td class="text_nowrap">15.11.2018, 12:32</td>
                                <td>
                                    <a class="btn_play" href="">
                                        <img src="assets/img/icon__play.svg" class="img-fluid" alt="">
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td>003</td>
                                <td class="table_long"><a href="#">75@ya.ru</a></td>
                                <td>155</td>
                                <td class="text_nowrap">15.11.2018, 12:32</td>
                                <td>
                                    <a class="btn_play" href="">
                                        <img src="assets/img/icon__play.svg" class="img-fluid" alt="">
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td>004</td>
                                <td class="table_long"><a href="#">75@ya.ru</a></td>
                                <td>155</td>
                                <td class="text_nowrap">15.11.2018, 12:32</td>
                                <td>
                                    <a class="btn_play" href="">
                                        <img src="assets/img/icon__play.svg" class="img-fluid" alt="">
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td>005</td>
                                <td class="table_long"><a href="#">75@ya.ru</a></td>
                                <td>155</td>
                                <td class="text_nowrap">15.11.2018, 12:32</td>
                                <td>
                                    <a class="btn_play" href="">
                                        <img src="assets/img/icon__play.svg" class="img-fluid" alt="">
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td>006</td>
                                <td class="table_long"><a href="#">75@ya.ru</a></td>
                                <td>155</td>
                                <td class="text_nowrap">15.11.2018, 12:32</td>
                                <td>
                                    <a class="btn_play" href="">
                                        <img src="assets/img/icon__play.svg" class="img-fluid" alt="">
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td>007</td>
                                <td class="table_long"><a href="#">75@ya.ru</a></td>
                                <td>155</td>
                                <td class="text_nowrap">15.11.2018, 12:32</td>
                                <td>
                                    <a class="btn_play" href="">
                                        <img src="assets/img/icon__play.svg" class="img-fluid" alt="">
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td>008</td>
                                <td class="table_long"><a href="#">75@ya.ru</a></td>
                                <td>155</td>
                                <td class="text_nowrap">15.11.2018, 12:32</td>
                                <td>
                                    <a class="btn_play" href="">
                                        <img src="assets/img/icon__play.svg" class="img-fluid" alt="">
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td>009</td>
                                <td class="table_long"><a href="#">75@ya.ru</a></td>
                                <td>155</td>
                                <td class="text_nowrap">15.11.2018, 12:32</td>
                                <td>
                                    <a class="btn_play" href="">
                                        <img src="assets/img/icon__play.svg" class="img-fluid" alt="">
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td>010</td>
                                <td class="table_long"><a href="#">75@ya.ru</a></td>
                                <td>155</td>
                                <td class="text_nowrap">15.11.2018, 12:32</td>
                                <td>
                                    <a class="btn_play" href="">
                                        <img src="assets/img/icon__play.svg" class="img-fluid" alt="">
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td>011</td>
                                <td class="table_long"><a href="#">75@ya.ru</a></td>
                                <td>155</td>
                                <td class="text_nowrap">15.11.2018, 12:32</td>
                                <td>
                                    <a class="btn_play" href="">
                                        <img src="assets/img/icon__play.svg" class="img-fluid" alt="">
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td>012</td>
                                <td class="table_long"><a href="#">75@ya.ru</a></td>
                                <td>155</td>
                                <td class="text_nowrap">15.11.2018, 12:32</td>
                                <td>
                                    <a class="btn_play" href="">
                                        <img src="assets/img/icon__play.svg" class="img-fluid" alt="">
                                    </a>
                                </td>
                            </tr>

                        </table>
                    </div>

                    <ul class="pagination">
                        <li class="disable"><span><i class="fas fa-angle-left"></i></span></li>
                        <li class="active"><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">...</a></li>
                        <li><a href="#">15</a></li>
                        <li><a href="#">16</a></li>
                        <li><a href="#"><i class="fas fa-angle-right"></i></a></li>
                    </ul>

                </div>
            </section>

            <?php include ('inc/footer.inc.php'); ?>

        </div>

        <?php include ('inc/scripts.inc.php'); ?>

    </body>
</html>
