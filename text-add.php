<!doctype html>
<html class="no-js" lang="">

    <?php include ('inc/head.inc.php'); ?>

    <body>

        <div class="page">

            <?php include ('inc/header.inc.php'); ?>

            <section class="main">
                <div class="container">

                    <div class="heading">
                        <ul class="heading__nav">
                            <li class="active"><a href="#">Условия использования</a></li>
                            <li><a href="#">Новости для покупателей</a></li>
                            <li><a href="#">Новости для Для продавца</a></li>
                            <li><a href="#">Статьи 10 баллов за прочтение</a></li>
                        </ul>

                        <div class="heading__row">
                            <div class="heading__col">
                                <h1>ДОБАВЛЕНИЕ/РЕДАКТИРОВАНИЕ ТЕКСТА</h1>
                            </div>
                            <div class="heading__col">
                                <a href="#" class="color_red"><strong>удалить</strong></a>
                            </div>
                        </div>
                    </div>


                    <div class="posting">
                        <div class="form_group">
                            <input class="form_control" name="name" placeholder="Название">
                        </div>
                        <div class="form_group">
                            <textarea class="form_control" name="message" placeholder="Добавление текста" rows="8"></textarea>
                        </div>
                        <div class="posting__bottom">
                            <div class="posting__type">
                                <div class="posting__type_title">Тип</div>
                                <ul class="posting__type_list">
                                    <li>
                                        <label class="form_radio">
                                            <input type="radio" name="posting_type">
                                            <span>Страница</span>
                                        </label>
                                    </li>
                                    <li>
                                        <label class="form_radio">
                                            <input type="radio" name="posting_type" checked>
                                            <span>Новость</span>
                                        </label>
                                        <ul>
                                            <li>
                                                <label class="form_checkbox">
                                                    <input type="checkbox" name="check" checked>
                                                    <span>Новость для провизора</span>
                                                </label>
                                            </li>
                                            <li>
                                                <label class="form_checkbox">
                                                    <input type="checkbox" name="check" checked>
                                                    <span>Новость для покупателя</span>
                                                </label>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <label class="form_radio">
                                            <input type="radio" name="posting_type">
                                            <span>10 баллов за прочтение  для продовца</span>
                                        </label>
                                    </li>
                                </ul>
                            </div>
                            <div type="posting__button">
                                <button type="submit" class="btn">Сохранить</button>
                            </div>
                        </div>
                    </div>


                </div>
            </section>

            <?php include ('inc/footer.inc.php'); ?>

        </div>

        <?php include ('inc/scripts.inc.php'); ?>

    </body>
</html>
