<!doctype html>
<html class="no-js" lang="">

    <?php include ('inc/head.inc.php'); ?>

    <body>

        <div class="page">

            <?php include ('inc/header.inc.php'); ?>

            <section class="main">
                <div class="container">

                    <div class="heading">
                        <ul class="heading__nav">
                            <li><a href="#">Все</a></li>
                            <li><a href="#">Покупатели</a></li>
                            <li><a href="#">Провизоры</a></li>
                            <li class="active"><a href="#">Аптечные сети</a></li>
                        </ul>
                        <div class="heading__row">
                            <div class="heading__col">
                                <h1>АПТЕЧНЫЕ СЕТИ</h1>
                            </div>
                        </div>
                    </div>

                    <div class="profile">

                        <div class="profile__nav">

                        </div>

                        <div class="profile__content">

                            <div class="profile__heading">Добавить аптечную сеть</div>

                            <div class="profile__content_wrap">

                                <div class="profile__group">
                                    <div class="inline">
                                        <div class="inline__left"></div>
                                        <div class="inline__right"><h3>Основные данные</h3></div>
                                    </div>

                                    <div class="inline form_group">
                                        <div class="inline__left">
                                            <label class="form_label">Название сети</label>
                                        </div>
                                        <div class="inline__right">
                                            <input type="text" class="form_control" name="email" placeholder="" value="">
                                        </div>
                                    </div>

                                    <div class="inline form_group">
                                        <div class="inline__left">
                                            <label class="form_label">E-mail для входа</label>
                                        </div>
                                        <div class="inline__right">
                                            <input type="text" class="form_control" name="email" placeholder="" value="">
                                        </div>
                                    </div>

                                    <div class="inline form_group">
                                        <div class="inline__left">
                                            <label class="form_label">ПАРОЛЬ</label>
                                        </div>
                                        <div class="inline__right">
                                            <input type="password" class="form_control" name="email" placeholder="" value="">
                                        </div>
                                    </div>
                                    <div class="inline form_group">
                                        <div class="inline__left">
                                            <label class="form_label">ПОВТОРИТЬ ПАРОЛЬ</label>
                                        </div>
                                        <div class="inline__right">
                                            <input type="password" class="form_control" name="email" placeholder="" value="">
                                        </div>
                                    </div>

                                    <div class="inline">
                                        <div class="inline__left"></div>
                                        <div class="inline__right">
                                            <button type="submit" class="btn btn__rounded">Добавить аптечную сеть</button>
                                        </div>
                                    </div>
                                </div>

                            </div>


                        </div>

                    </div>

                </div>
            </section>

            <?php include ('inc/footer.inc.php'); ?>

        </div>

        <?php include ('inc/scripts.inc.php'); ?>

    </body>
</html>
