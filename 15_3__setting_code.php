<!doctype html>
<html class="no-js" lang="">

    <?php include ('inc/head.inc.php'); ?>

    <body>

        <div class="page">

            <?php include ('inc/header.inc.php'); ?>

            <section class="main">
                <div class="container">

                    <div class="heading">
                        <h1>ОБЩИЕ НАСТРОЙКИ ДЛЯ ВСЕХ</h1>
                    </div>

                    <div class="profile">

                        <div class="profile__nav">
                            <ul>
                                <li><a href="#"><span>Уведомления первостольникам</span></a></li>
                                <li><a href="#"><span>Уведомления покупателям</span></a></li>
                                <li><a href="#"><span>Настройка баллов</span></a></li>
                                <li class="active"><a href="#"><span>Коды товарной группы</span></a></li>
                            </ul>
                        </div>

                        <div class="profile__content">

                            <div class="profile__heading">КОДЫ ТОВАРНОЙ ГРУППЫ</div>

                            <div class="table_responsive">
                                <table class="table_form mb_20">
                                    <tr>
                                        <th class="text_center">Код</th>
                                        <th class="text_nowrap">товарная группа</th>
                                        <th colspan="2" class="text_nowrap">кОЛИЧЕСТВО БАЛЛОВ</th>
                                    </tr>
                                    <tr>
                                        <td  class="mw_70">
                                            <div class="mw_50">
                                                <input class="form_control form_control_sm text_center" type="text" name="name" placeholder="" value="55">
                                            </div>
                                        </td>
                                        <td class="table_long">
                                            <input class="form_control form_control_sm" type="text" name="name" placeholder="Название кода товарной группы" value="">
                                        </td>
                                        <td>
                                            <input class="form_control form_control_sm text_center" type="text" name="name" placeholder="" value="55">
                                        </td>
                                        <td>
                                            <button type="button" class="btn btn_sm">-</button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td  class="mw_70">
                                            <div class="mw_50">
                                                <input class="form_control form_control_sm text_center" type="text" name="name" placeholder="" value="55">
                                            </div>
                                        </td>
                                        <td class="table_long">
                                            <input class="form_control form_control_sm" type="text" name="name" placeholder="Название кода товарной группы" value="">
                                        </td>
                                        <td>
                                            <input class="form_control form_control_sm text_center" type="text" name="name" placeholder="" value="55">
                                        </td>
                                        <td>
                                            <button type="button" class="btn btn_sm">-</button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td  class="mw_70">
                                            <div class="mw_50">
                                                <input class="form_control form_control_sm text_center" type="text" name="name" placeholder="" value="55">
                                            </div>
                                        </td>
                                        <td class="table_long">
                                            <input class="form_control form_control_sm" type="text" name="name" placeholder="Название кода товарной группы" value="">
                                        </td>
                                        <td>
                                            <input class="form_control form_control_sm text_center" type="text" name="name" placeholder="" value="55">
                                        </td>
                                        <td>
                                            <button type="button" class="btn btn_sm">-</button>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td  class="mw_70">
                                            <div class="mw_50">
                                                <input class="form_control form_control_sm text_center" type="text" name="name" placeholder="" value="55">
                                            </div>
                                        </td>
                                        <td class="table_long">
                                            <input class="form_control form_control_sm" type="text" name="name" placeholder="Название кода товарной группы" value="">
                                        </td>
                                        <td>
                                            <input class="form_control form_control_sm text_center" type="text" name="name" placeholder="" value="55">
                                        </td>
                                        <td>
                                            <button type="button" class="btn btn_sm">+</button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="mw_70"></td>
                                        <td colspan="3">
                                            <button type="submit" class="btn">Сохранить</button>
                                        </td>
                                    </tr>
                                </table>
                            </div>

                        </div>

                    </div>

                </div>
            </section>

            <?php include ('inc/footer.inc.php'); ?>

        </div>

        <?php include ('inc/scripts.inc.php'); ?>

    </body>
</html>
