<!doctype html>
<html class="no-js" lang="">

    <?php include ('inc/head.inc.php'); ?>

    <body>

        <div class="page">

            <?php include ('inc/header.inc.php'); ?>

            <section class="main">
                <div class="container">

                    <div class="heading">
                        <div class="heading__subtitle">Покупатель</div>
                        <div class="heading__row">
                            <div class="heading__col">
                                <h1 class="text_lowercase">Alex@gmail.com</h1>
                            </div>
                        </div>
                    </div>

                    <div class="profile">

                        <div class="profile__nav">
                            <ul>
                                <li class="active"><a href="#"><span>Инфо</span></a></li>
                                <li><a href="#"><span>Зачисление/списание баллов</span></a></li>
                                <li><a href="#"><span>История входа</span></a></li>
                            </ul>
                        </div>

                        <div class="profile__content">

                            <div class="profile__heading">Сводная информация о пользователе 156$ | 356Uli</div>

                            <div class="profile__content_wrap">


                                <div class="profile__group">
                                    <div class="inline">
                                        <div class="inline__left"></div>
                                        <div class="inline__right">
                                            <h3>О пользователе</h3>
                                        </div>
                                    </div>
                                    <div class="profile__about">
                                        <div class="inline">
                                            <div class="inline__left"><strong>Тип</strong></div>
                                            <div class="inline__right">покупатель</div>
                                        </div>
                                        <div class="inline">
                                            <div class="inline__left"><strong>Язык пользователя</strong></div>
                                            <div class="inline__right">EN</div>
                                        </div>
                                        <div class="inline">
                                            <div class="inline__left"><strong>Дата регистрации</strong></div>
                                            <div class="inline__right">15.11.2018; 11:43;</div>
                                        </div>
                                        <div class="inline">
                                            <div class="inline__left"><strong>Последний вход</strong></div>
                                            <div class="inline__right">15.11.2018; 11:43;</div>
                                        </div>
                                    </div>
                                </div>

                                <div class="profile__group mb_40">
                                    <div class="inline">
                                        <div class="inline__left"></div>
                                        <div class="inline__right">
                                            <h3>ИНФОРМАЦИЯ О ПОКУПАТЕЛЕ</h3>
                                        </div>
                                    </div>

                                    <div class="inline form_group">
                                        <div class="inline__left">
                                            <label class="form_label">e-MAIL</label>
                                        </div>
                                        <div class="inline__right">
                                            <input type="text" class="form_control" name="email" placeholder="" value="alex928@gmail.com">
                                        </div>
                                    </div>

                                    <div class="inline form_group">
                                        <div class="inline__left">
                                            <label class="form_label">Имя</label>
                                        </div>
                                        <div class="inline__right">
                                            <input type="text" class="form_control" name="email" placeholder="" value="Алексей">
                                        </div>
                                    </div>

                                    <div class="inline form_group">
                                        <div class="inline__left">
                                            <label class="form_label">Почтовый адрес для подарков</label>
                                        </div>
                                        <div class="inline__right">
                                            <textarea class="form_control" name="address" placeholder="Пример: Московская область, г. Москва, ул. Пушкина 32-123, индекс 000912, Иванов Иван Иванович" rows="2"></textarea>
                                        </div>
                                    </div>

                                    <div class="inline">
                                        <div class="inline__left"></div>
                                        <div class="inline__right">
                                            <button type="submit" class="btn btn__rounded">Сохранить адрес</button>
                                        </div>
                                    </div>

                                </div>

                                <div class="profile__group">
                                    <div class="inline">
                                        <div class="inline__left"></div>
                                        <div class="inline__right"><h3>Новый пароль</h3></div>
                                    </div>

                                    <div class="inline">
                                        <div class="inline__left"></div>
                                        <div class="inline__right"></div>
                                    </div>
                                    <div class="inline form_group">
                                        <div class="inline__left">
                                            <label class="form_label">нОВЫЙ ПАРОЛЬ</label>
                                        </div>
                                        <div class="inline__right">
                                            <input type="password" class="form_control" name="email" placeholder="" value="">
                                        </div>
                                    </div>
                                    <div class="inline form_group">
                                        <div class="inline__left">
                                            <label class="form_label">ПОВТОРИТЬ ПАРОЛЬ</label>
                                        </div>
                                        <div class="inline__right">
                                            <input type="password" class="form_control" name="email" placeholder="" value="">
                                        </div>
                                    </div>

                                    <div class="inline">
                                        <div class="inline__left"></div>
                                        <div class="inline__right">
                                            <button type="submit" class="btn btn__rounded">Сохранить новый пароль</button>
                                        </div>
                                    </div>
                                </div>

                            </div>


                        </div>

                    </div>

                </div>
            </section>

            <?php include ('inc/footer.inc.php'); ?>

        </div>

        <?php include ('inc/scripts.inc.php'); ?>

    </body>
</html>
