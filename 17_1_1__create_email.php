<!doctype html>
<html class="no-js" lang="">

    <?php include ('inc/head.inc.php'); ?>

    <body>

        <div class="page">

            <?php include ('inc/header.inc.php'); ?>

            <section class="main">
                <div class="container">

                    <div class="heading">
                        <ul class="heading__nav">
                            <li class="active"><a href="#">Рассылки E-mail</a></li>
                            <li><a href="#">Рассылки SMS</a></li>
                        </ul>

                        <div class="heading__row">
                            <h1>Создать/просмотреть письмо</h1>
                        </div>
                    </div>

                    <div class="profile">
                        <div class="profile__content">
                            <div class="inline form_group">
                                <div class="inline__left">
                                    <label class="form_label">КОму</label>
                                </div>
                                <div class="inline__right">
                                    <ul class="input_list">
                                        <li>
                                            <label class="form_radio">
                                                <input type="radio" name="radio" value="" checked>
                                                <span>Всем</span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="form_radio">
                                                <input type="radio" name="radio" value="">
                                                <span>Только покупателям</span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="form_radio">
                                                <input type="radio" name="radio" value="">
                                                <span>Только первостольникам</span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="form_radio">
                                                <input type="radio" name="radio" value="">
                                                <span>Только Аптечным сетям</span>
                                            </label>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="inline form_group">
                                <div class="inline__left">
                                    <label class="form_label">Тема письма</label>
                                </div>
                                <div class="inline__right">
                                    <input type="text" class="form_control" name="name" placeholder="">
                                </div>
                            </div>
                            <div class="inline form_group">
                                <div class="inline__left">
                                    <label class="form_label">Для провизоров</label>
                                </div>
                                <div class="inline__right">
                                    <textarea class="form_control" name="message" placeholder="Только текст" rows="10"></textarea>
                                </div>
                            </div>
                            <ul class="btn_group btn_group_right">
                                <li>
                                    <button type="button" class="btn_text">Отправить тестовое</button>
                                </li>
                                <li>
                                    <button type="submit" class="btn">Отправить</button>
                                </li>
                            </ul>
                        </div>

                    </div>


                </div>
            </section>

            <?php include ('inc/footer.inc.php'); ?>

        </div>

        <?php include ('inc/scripts.inc.php'); ?>

    </body>
</html>
