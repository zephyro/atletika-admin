<!doctype html>
<html class="no-js" lang="">

    <?php include ('inc/head.inc.php'); ?>

    <body>

        <div class="page">

            <?php include ('inc/header.inc.php'); ?>

            <section class="main">
                <div class="container">

                    <div class="heading">
                        <div class="heading__subtitle">Провизор</div>
                        <div class="heading__row">
                            <div class="heading__col">
                                <h1><span class="text_lowercase">Alex@gmail.com</span></h1>
                            </div>
                            <div class="heading__col">
                                <div class="select">
                                    <div class="select__active">
                                        <span>Выберите действие</span>
                                    </div>
                                    <ul class="select__dropdown">
                                        <li><a href="#">Разрешить работу</a></li>
                                        <li><a href="#">Запретить работу</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="profile">

                        <div class="profile__nav">
                            <ul>
                                <li><a href="#"><span>Инфо</span></a></li>
                                <li class="active"><a href="#"><span>Зачисление/списание баллов</span></a></li>
                                <li><a href="#"><span>Подарочные карты</span></a></li>
                                <li><a href="#"><span>История входа</span></a></li>
                            </ul>
                        </div>

                        <div class="profile__content">


                            <div class="profile__heading">Информация о баллах пользователя</div>

                            <div class="points"><strong>Текущее количество баллов: </strong> <span>392</span></div>
                            <ul class="heading__nav">
                                <li><a href="#">Начисление</a></li>
                                <li class="active"><a href="#">Списание</a></li>
                            </ul>

                            <div class="table_responsive">
                                <table class="table_strip table_strip_rose">
                                    <tr>
                                        <th class="text_uppercase">Списано</th>
                                        <th class="text_uppercase">Дата</th>
                                    </tr>
                                    <tr>
                                        <td class="text_center"><strong>32</strong></td>
                                        <td class="text_center text_nowrap">11.12.2019,11:26</td>
                                    </tr>
                                    <tr>
                                        <td class="text_center"><strong>32</strong></td>
                                        <td class="text_center text_nowrap">11.12.2019,11:26</td>
                                    </tr>
                                    <tr>
                                        <td class="text_center"><strong>32</strong></td>
                                        <td class="text_center text_nowrap">11.12.2019,11:26</td>
                                    </tr>
                                    <tr>
                                        <td class="text_center"><strong>32</strong></td>
                                        <td class="text_center text_nowrap">11.12.2019,11:26</td>
                                    </tr>
                                    <tr>
                                        <td class="text_center"><strong>32</strong></td>
                                        <td class="text_center text_nowrap">11.12.2019,11:26</td>
                                    </tr>
                                    <tr>
                                        <td class="text_center"><strong>32</strong></td>
                                        <td class="text_center text_nowrap">11.12.2019,11:26</td>
                                    </tr>
                                    <tr>
                                        <td class="text_center"><strong>32</strong></td>
                                        <td class="text_center text_nowrap">11.12.2019,11:26</td>
                                    </tr>
                                    <tr>
                                        <td class="text_center"><strong>32</strong></td>
                                        <td class="text_center text_nowrap">11.12.2019,11:26</td>
                                    </tr>
                                    <tr>
                                        <td class="text_center"><strong>32</strong></td>
                                        <td class="text_center text_nowrap">11.12.2019,11:26</td>
                                    </tr>
                                </table>
                            </div>

                        </div>

                    </div>

                </div>
            </section>

            <?php include ('inc/footer.inc.php'); ?>

        </div>

        <?php include ('inc/scripts.inc.php'); ?>

    </body>
</html>
