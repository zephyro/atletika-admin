<!doctype html>
<html class="no-js" lang="">

    <?php include ('inc/head.inc.php'); ?>

    <body>

        <div class="page">

            <?php include ('inc/header.inc.php'); ?>

            <section class="main">
                <div class="container">

                    <div class="heading">
                        <div class="heading__subtitle">Продавец</div>
                        <div class="heading__row">
                            <div class="heading__col">
                                <h1><span class="text_lowercase">Alex@gmail.com</span></h1>
                            </div>
                            <div class="heading__col">
                                <div class="select">
                                    <div class="select__active">
                                        <span>Выберите действие</span>
                                    </div>
                                    <ul class="select__dropdown">
                                        <li><a href="#">Разрешить работу</a></li>
                                        <li><a href="#">Запретить работу</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="profile">

                        <div class="profile__nav">
                            <ul>
                                <li><a href="#"><span>Инфо</span></a></li>
                                <li><a href="#"><span>Зачисление/списание баллов</span></a></li>
                                <li><a href="#"><span>Подарочные карты</span></a></li>
                                <li class="active"><a href="#"><span>История входа</span></a></li>
                            </ul>
                        </div>

                        <div class="profile__content">


                            <div class="profile__heading">Сертификаты Первостольник</div>

                        <div class="table_responsive">
                                <table class="table_strip">
                                    <tr>
                                        <th class="text_uppercase table_long text_left">название</th>
                                        <th class="text_uppercase">баллов</th>
                                        <th class="text_uppercase text_nowrap">Сумма в рублях</th>
                                        <th class="text_uppercase">Дата</th>
                                        <th class="text_uppercase">Скачать</th>
                                    </tr>
                                    <tr>
                                        <td class="text_uppercase text_nowrap"><strong>За январь</strong></td>
                                        <td class="text_center"><strong>32</strong></td>
                                        <td class="text_nowrap text_center">1560</td>
                                        <td class="text_nowrap text_center">11.12.2019,11:26</td>
                                        <td>
                                            <a href="#" class="btn btn_border_blue btn__rounded btn_pdf">
                                                <span>скачать</span>
                                                <i>
                                                    <svg class="ico-svg" viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="assets/img/sprite_icons.svg#icon__pdf" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </i>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text_uppercase text_nowrap"><strong>За январь</strong></td>
                                        <td class="text_center"><strong>32</strong></td>
                                        <td class="text_nowrap text_center">1560</td>
                                        <td class="text_nowrap text_center">11.12.2019,11:26</td>
                                        <td>
                                            <a href="#" class="btn btn_border_blue btn__rounded btn_pdf">
                                                <span>скачать</span>
                                                <i>
                                                    <svg class="ico-svg" viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="assets/img/sprite_icons.svg#icon__pdf" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </i>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text_uppercase text_nowrap"><strong>За январь</strong></td>
                                        <td class="text_center"><strong>32</strong></td>
                                        <td class="text_nowrap text_center">1560</td>
                                        <td class="text_nowrap text_center">11.12.2019,11:26</td>
                                        <td>
                                            <a href="#" class="btn btn_border_blue btn__rounded btn_pdf">
                                                <span>скачать</span>
                                                <i>
                                                    <svg class="ico-svg" viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="assets/img/sprite_icons.svg#icon__pdf" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </i>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text_uppercase text_nowrap"><strong>За январь</strong></td>
                                        <td class="text_center"><strong>32</strong></td>
                                        <td class="text_nowrap text_center">1560</td>
                                        <td class="text_nowrap text_center">11.12.2019,11:26</td>
                                        <td>
                                            <a href="#" class="btn btn_border_blue btn__rounded btn_pdf">
                                                <span>скачать</span>
                                                <i>
                                                    <svg class="ico-svg" viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="assets/img/sprite_icons.svg#icon__pdf" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </i>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text_uppercase text_nowrap"><strong>За январь</strong></td>
                                        <td class="text_center"><strong>32</strong></td>
                                        <td class="text_nowrap text_center">1560</td>
                                        <td class="text_nowrap text_center">11.12.2019,11:26</td>
                                        <td>
                                            <a href="#" class="btn btn_border_blue btn__rounded btn_pdf">
                                                <span>скачать</span>
                                                <i>
                                                    <svg class="ico-svg" viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="assets/img/sprite_icons.svg#icon__pdf" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </i>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text_uppercase text_nowrap"><strong>За январь</strong></td>
                                        <td class="text_center"><strong>32</strong></td>
                                        <td class="text_nowrap text_center">1560</td>
                                        <td class="text_nowrap text_center">11.12.2019,11:26</td>
                                        <td>
                                            <a href="#" class="btn btn_border_blue btn__rounded btn_pdf">
                                                <span>скачать</span>
                                                <i>
                                                    <svg class="ico-svg" viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="assets/img/sprite_icons.svg#icon__pdf" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </i>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text_uppercase text_nowrap"><strong>За январь</strong></td>
                                        <td class="text_center"><strong>32</strong></td>
                                        <td class="text_nowrap text_center">1560</td>
                                        <td class="text_nowrap text_center">11.12.2019,11:26</td>
                                        <td>
                                            <a href="#" class="btn btn_border_blue btn__rounded btn_pdf">
                                                <span>скачать</span>
                                                <i>
                                                    <svg class="ico-svg" viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="assets/img/sprite_icons.svg#icon__pdf" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </i>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text_uppercase text_nowrap"><strong>За январь</strong></td>
                                        <td class="text_center"><strong>32</strong></td>
                                        <td class="text_nowrap text_center">1560</td>
                                        <td class="text_nowrap text_center">11.12.2019,11:26</td>
                                        <td>
                                            <a href="#" class="btn btn_border_blue btn__rounded btn_pdf">
                                                <span>скачать</span>
                                                <i>
                                                    <svg class="ico-svg" viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="assets/img/sprite_icons.svg#icon__pdf" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </i>
                                            </a>
                                        </td>
                                    </tr>

                                </table>
                            </div>

                        </div>

                    </div>

                </div>
            </section>

            <?php include ('inc/footer.inc.php'); ?>

        </div>

        <?php include ('inc/scripts.inc.php'); ?>

    </body>
</html>
